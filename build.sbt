
import sbt.Keys._
import sbt._

// https://mvnrepository.com/artifact/org.apache.spark/spark-core_2.10
val lib_SparkCore = "org.apache.spark" % "spark-core_2.10" % "2.0.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-graphx_2.10
val lib_GraphX = "org.apache.spark" % "spark-graphx_2.10" % "2.0.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-sql_2.10
val lib_SparkSQL = "org.apache.spark" % "spark-sql_2.10" % "2.0.0"

// GraphFrame
val lib_GraphFrame = "graphframes" % "graphframes" % "0.2.0-spark2.0-s_2.11"

// https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
val lib_log4j_core = "org.apache.logging.log4j" % "log4j-core" % "2.7"

// https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api/2.6.2
val lib_log4j_api = "org.apache.logging.log4j" % "log4j-api" % "2.6.2"

// define the common properties for all the projects
lazy val commonSettings = Seq(
    organization := "org.fedcentric",
    version := "1.0",
    scalaVersion := "2.11.8"
)

/**
 * Project of application based on spark and graph lib.
 * the val name is the projectID, in file(XXX), XXX is the path
 * lazy val root = (project in file(".")).
 */

lazy val graphApp = (project in file("GraphApp")).
    settings(commonSettings: _*).
    settings(
        name := "graphApp",
        // libraryDependencies: SettingKey[Seq[ModuleID]]
        libraryDependencies ++= Seq(lib_SparkCore, lib_GraphX, lib_SparkSQL)
    ).
    settings(
        resolvers += "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven",
        libraryDependencies += lib_GraphFrame
    ).
    settings(
        mainClass in (Compile, run) := Some("org.fedcentric.spark.thousandGenome.RunGraph")
    )

// project for benchmarking of Spark on HDFS
lazy val HDFSBenchMark = (project in file("HDFSTest")).
    settings(commonSettings: _*).
    settings(
        name := "HDFSBenchMark",

        // libraryDependencies: SettingKey[Seq[ModuleID]]
        libraryDependencies ++= Seq(lib_SparkCore, lib_GraphX, lib_SparkSQL, lib_log4j_api, lib_log4j_core)
    ).
    settings(
        mainClass in (Test, run) := Some("testRead")
    ).
    settings(
        mainClass in (Runtime, run) := Some("testWrite")
    )

lazy val LogParser = (project in file("LogParser")).
    settings(commonSettings: _*).
    settings(
        name := "LogParser",

        // libraryDependencies: SettingKey[Seq[ModuleID]]
        libraryDependencies ++= Seq(lib_SparkCore, lib_SparkSQL)
    ).
    settings(
        mainClass in (Test, run) := Some("SparkHDFSLogParser")
    )