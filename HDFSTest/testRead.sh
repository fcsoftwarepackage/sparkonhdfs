#!/bin/bash
sbt package
for (( i=0;i<$1;i++ ))
do
  /opt/spark-2.0/bin/spark-submit \
  --jars $(echo lib/apache-log4j-2.7-bin/*.jar | tr ' ' ',')\
  --master "spark://marvin1:7077"\
  --class ReadTest target/scala-2.11/sparkprojects_2.11-1.0.jar
done


