import org.apache.log4j.LogManager
import org.apache.log4j.Logger
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.LoggerContext
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory
import org.apache.logging.log4j.core.config.builder.api.LayoutComponentBuilder
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration 
import org.apache.spark.sql.SparkSession

object ReadTest {
  def main(args: Array[String]) {
    var builder : ConfigurationBuilder[BuiltConfiguration] = ConfigurationBuilderFactory.newConfigurationBuilder();

    builder.setStatusLevel( Level.ERROR);
    builder.setConfigurationName("RollingBuilder");
    // create a console appender
    var appenderBuilder : AppenderComponentBuilder = builder.newAppender("Stdout", "CONSOLE").addAttribute("target", "System.out")
      //ConsoleAppender.Target.SYSTEM_OUT); // can't access like this in scala
    appenderBuilder.add(builder.newLayout("PatternLayout")
      .addAttribute("pattern", "%d [%t] %-5level: %msg%n%throwable"));
    builder.add( appenderBuilder );
    // create a rolling file appender
    var layoutBuilder : LayoutComponentBuilder = builder.newLayout("PatternLayout")
      .addAttribute("pattern", "%d [%t] %-5level: %msg%n");
    var triggeringPolicy = builder.newComponent("Policies")
    triggeringPolicy = triggeringPolicy.addComponent(builder.newComponent("CronTriggeringPolicy").addAttribute("schedule", "0 0 0 * * ?"))
    triggeringPolicy = triggeringPolicy.addComponent(builder.newComponent("SizeBasedTriggeringPolicy").addAttribute("size", "100M"));
    appenderBuilder = builder.newAppender("rolling", "RollingFile")
      .addAttribute("fileName", "target/rolling.log")
      .addAttribute("filePattern", "target/archive/rolling-%d{MM-dd-yy}.log.gz")
      .add(layoutBuilder)
      .addComponent(triggeringPolicy);
    builder.add(appenderBuilder);

    // create the new logger
    var temp_builder = builder.newLogger( "TestLogger", Level.DEBUG )
    temp_builder = temp_builder.add( builder.newAppenderRef( "rolling" ) )
    temp_builder = temp_builder.addAttribute( "additivity", false );
    builder.add(temp_builder)

    builder.add( builder.newRootLogger(Level.DEBUG )
        .add( builder.newAppenderRef( "rolling" ) ) );
    var ctx : LoggerContext = Configurator.initialize(builder.build());

    val log = ctx.getLogger("TestLogger");

    val spark = SparkSession
      .builder
      .appName("HDFS Write Test")
      .getOrCreate()

    val sc = spark.sparkContext

    def time[R](block: => R): Long = {
      val t0 = System.nanoTime()
      val result = block    // call-by-name
      val t1 = System.nanoTime()
      t1 - t0
    }

    //log.error("TEST")
    
    val f = sc.textFile(
      "hdfs://10.1.10.40/VCFs/ALL.chr6.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf")

    val time_ns = time {
      f.count
    }

    val nano = scala.math.pow(10, -9);

    log.info("READ: " + (time_ns * nano) + " s");
    
    spark.stop()
  }
}
