#!/bin/bash
export JAVA_HOME=/usr/java/jdk1.8.0_101
export PATH=$JAVA_HOME/bin:$PATH
export SPARK_CONF_DIR=`pwd`/SparkConf

LOGFILE=`grep -Po 'RollingAppenderU.File=\K.+' SparkConf/log4j.properties`

driver_memory=25G
driver_cores=4
total_executor_cores=12
executor_cores=1
executor_memory=50G

num_iterations=1

while true;
do
  case "$1" in
    --driver-memory) driver_memory="$2"; shift 2 ;;
    --driver-cores) driver_cores="$2"; shift 2 ;;
    --total-executor-cores) total_executor_cores="$2"; shift 2 ;;
    --executor-cores) executor_cores="$2"; shift 2 ;;
    --executor-memory) executor_memory="$2"; shift 2 ;;
    --num-iterations) num_iterations="$2"; shift 2 ;;
    --infile) INFILE="$2"; shift 2 ;;
    --outfile) OUTFILE="$2"; shift 2 ;;
    --) shift; break ;;
    *) break ;;
  esac
done

echo -e "driver-memory=$driver_memory,\
driver-cores=$driver_cores,\
total-executor-cores=$total_executor_cores,\
executor-cores=$executor_cores,\
executor-memory=$executor_memory" >> "$LOGFILE"

echo "Running the file by spark-submit ..."
_now="$(sed -e 's/\//_/g;s/:/_/g' <<<$(date +'%D_%T'))"

for iterate in `seq 1 $num_iterations`;
do
   /opt/spark-2.0/bin/spark-submit \
         --name "Cluster Test of Graph" \
         --class "org.fedcentric.spark.thousandGenome.RunGraph" \
         --master spark://10.1.10.40:7077 \
         --driver-memory $driver_memory \
         --driver-cores $driver_cores \
         --total-executor-cores $total_executor_cores \
         --executor-cores $executor_cores \
         --executor-memory $executor_memory \
         --conf spark.driver.maxResultSize=20G \
         --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
         --conf spark.kryoserializer.buffer.max=256m \
         --packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 \
       target/scala-2.11/graphapp_2.11-1.0.jar \
       $INFILE $OUTFILE
done
