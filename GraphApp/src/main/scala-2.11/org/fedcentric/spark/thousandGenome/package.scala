package org.fedcentric.spark

import _root_.org.apache.spark.broadcast.Broadcast
import _root_.org.apache.spark.rdd.RDD
import java.io.Serializable
import scala.collection.Map
import scala.collection.mutable.ArrayBuffer

/**
 * This package object contains all the self-defined types which are used in package "thousandGenome"
 * Notice that the types including two versions, graphFrame and GraphX.
 * These self-defined types will be used in all the models, eg, parsing, graphProducer
 */
package object thousandGenome {

    /**
     * DataStructure, containing all the parsed metaData, Header and RawBody part.
     * @param InfoKeys All the possible Info keys shown on the metadata of VCF file
     * @param SampleIds All the Person IDs in the header line
     * @param HeaderLine Headerline containing all the header information about columns
     * @param VCFBody RDD[String]
     */
    case class VCF(InfoKeys:Broadcast[Set[String]],
                   SampleIds:Broadcast[Array[String]],
                   HeaderLine:String,
                   VCFBody:RDD[String])

    /**
     * Convert the information column in each line into a key->value pairs.
     * The number of Array[String] depend on the number of alt allele and artifacts in VCF file.
     */
    type InfoColumnDict = Map[String, Array[String]]

    /**
     * Type of parsed result, each line in body part of VCF will be converted into a tuple
     * @Map[String, String] stores the chromosome, reference allele and starting location
     * @Array[(Map[String,String], Array[String], Array[String])]
     * Size of this array is determined by the number of alt alleles in this line.
     * The first Map[String,String] stores the info column as key-value pairs for each alt allele,
     * Second Array[String] stores the individuals who have this alt allele in heterozygous genotype
     * The third Array[String] stores the individuals who have this alt allele in homogeneous genotype
     */
    type ParsedLine = (Map[String, String], Array[(Map[String,String], Array[String], Array[String])])

    /**
     * look-up table for getting individual vertexID in the graph, eg, given HG000096, it will return
     * the corresponding vertexID which is a unique identifier in graph.
     */
    type Sample_ID = Map[String, scala.Long]

    /**
     * BroadCast version of Sample_ID. Will be used in Map function
     */
    type BroadCast_Sample_ID = Broadcast[Map[String, scala.Long]]
    /**
     * For heterozygous
     * Temporary structure for storing all the individuals for every alt alleles
     */
    type Allele_Ind_hete = Array[ArrayBuffer[String]]

    /**
     * For homogeneous
     * Temporary structure for storing all the individuals for every alt alleles
     */
    type Allele_Ind_homo = Array[ArrayBuffer[String]]

    /**
     * The look-up table to check population vertexID in the graph
     */
    type PopId = Map[String, scala.Long]


    /**
     * When a node or edge is defined, its non-numerical information (Strings
     * and not id Longs) will be stored in some type of class defined below.
     * All of these classes are subclasses of an abstract case class called
     * either abs_Node or abs_Edge.
     *
     * Below that, in the package object type_list, there are two types, called
     * Node and Edge.  These types will contain the id numbers of Nodes and
     * their information (stored as a subclass of abs_Node) or the source and
     * destination of an edge and their information (stored as a subclass of
     * abs_Edge).
     *
     * This allows us to avoid storing null values in nodes or edges.
     *
     */
    abstract class NodeParent(val nodetype: String, val name: String) extends Serializable

    case class Node_Pop(override val nodetype: String, override val name: String)
        extends NodeParent(nodetype, name)

    case class Node_SuperPop(override val nodetype: String, override val name: String)
        extends NodeParent(nodetype, name)

    case class Node_Ind(override val nodetype: String, override val name: String)
        extends NodeParent(nodetype, name)

    case class Node_Chrom(override val nodetype:String, override val name:String)
        extends NodeParent(nodetype, name)

    case class Node_Ref(override val nodetype: String,
                        override val name: String,
                        start: scala.Long,
                        stop: scala.Long)
        extends NodeParent(nodetype, name) with Serializable

    case class Node_Alt(override val nodetype: String,
                        override val name: String,
                        info: Map[String, String]
                           )
        extends NodeParent(nodetype, name)

    abstract class EdgeParent(val relationship: String) extends Serializable

    /* Define all types of edges here*/
    case class Edge_SuperPop_to_Pop(override val relationship: String)
        extends EdgeParent(relationship)

    case class Edge_Pop_to_Ind(override val relationship: String)
        extends EdgeParent(relationship)

    case class Edge_Chrom_to_Ref(override val relationship: String)
        extends EdgeParent(relationship)

    case class Edge_Ref_to_Alt(override val relationship: String, val variant: String)
        extends EdgeParent(relationship)

    case class Edge_Ind_to_Alt(override val relationship: String, val zygosity: String)
        extends EdgeParent(relationship)

    case class Edge_with_attr(override val relationship: String, val attr: String)
        extends EdgeParent(relationship)

    type NODE_ID = scala.Long
    type SRC_ID = scala.Long
    type DES_ID = scala.Long
    type NODE = (NODE_ID, NodeParent)
    type EDGE = (SRC_ID, DES_ID, EdgeParent)

    /**
     * GraphFrame version of node schema for converting RDD into Dataframe
     * @param id VertexID as an unique identifier
     * @param nodetype
     * @param name
     * @param start
     * @param stop
     * @param infoFeature
     */
    case class NodeDF(id: NODE_ID, nodetype: String, name: String, start: scala.Long = -1, stop: scala.Long = -1, infoFeature:String = null)

    /**
     * GraphFrame version of edge schema for converting RDD into Dataframe
     * @param src
     * @param dst
     * @param relationship
     * @param attr
     */
    case class EdgeDF(src: SRC_ID, dst: DES_ID, relationship: String, attr: String = null)

}
