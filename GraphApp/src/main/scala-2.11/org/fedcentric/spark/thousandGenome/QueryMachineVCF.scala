/**
 * Created by Tianwen on 7/31/2016.
 */

package org.fedcentric.spark.thousandGenome

import org.apache.spark.graphx._
import org.fedcentric.spark.graph.QueryMachine

class QueryMachineVCF(graph: Graph[NodeParent, EdgeParent]) extends QueryMachine {

    /**
     * Returns a sequence of all the neighbors of the first instance of a given
     * node.  Later this will need to be expanded to handle duplicate instances
     * of the given node.
     * @param nodetype The type of node that we want to find the neighbors of
     * @param name The name of the node that we want to find the neighbors of
     * @return A sequence of the names of the given node's neighbors
     */

    def findNeighbors(nodetype: String, name: String): Seq[Array[(VertexId, NodeParent)]]= {

        val direction = EdgeDirection.Either
        val desired = name
        val neighbors = graph.collectNeighbors(direction)
        val allele_pool = graph.vertices
            .filter(v => v._2.nodetype == nodetype)
            .filter(v => v._2.name == desired)
            .map(v => v._1)
            .first()

        val num = neighbors.lookup(allele_pool)
        return num
    }

    def findNodes(nodetype:String) = {

    }

    def spectralClusteringQuery(g:Any) = {

        import org.graphframes._

        import spark.implicits._

        val ndf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_final/Nodes/*")
        val edf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_final/Edges/*")
        val g = GraphFrame(ndf, edf)
        val Nodes_Refs_in_Range = g.vertices.filter($"nodetype" === "Ref_allele" && $"start" > "1" && $"start" < "5000000000".toLong).count

        val ndf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_levelOne_Consolidated/Nodes/*")
        val edf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_levelOne_Consolidated/Edges/*")
        val gg = GraphFrame(ndf, edf)
        val Nodes_Refs_in_Range = gg.vertices.filter($"nodetype" === "Ref_allele" && $"start" > "1" && $"start" < "5000000000".toLong).count

        val ndf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_Total_Consolidated/Nodes/*")
        val edf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_Total_Consolidated/Edges/*")
        val ggg = GraphFrame(ndf, edf)
        val Nodes_Refs_in_Range = ggg.vertices.filter($"nodetype" === "Ref_allele" && $"start" > "1" && $"start" < "5000000000".toLong).count


        /*Query1 and 2:*/
        gg.vertices.filter($"nodetype" === "Chrom" && $"name" === "1" ).count

        gg.edges.filter($"relationship" === "Chrom_to_ref").count
        gg.edges.filter($"src" !== "1000000000000".toLong).count

        //val Nodes_chroms = gg.vertices.filter($"nodetype" === "Chrom" && $"name" === "1" )

        //val chroms_id_broad = spark.sparkContext.broadcast(Nodes_chroms.rdd.map(r => r(0)).collect().toSet)
        //val Edges_chromToRef = gg.edges.filter($"relationship" === "Chrom_to_ref").filter(r => chroms_id_broad.value.contains(r(0)))
        //number of output rows: 3,116,531,690
        Edges_chromToRef.count()

        val Nodes_chroms = gg.vertices.filter($"nodetype" === "Chrom" && $"name" === "1" )
        val Edges_chromToRef = gg.edges.filter($"relationship" === "Chrom_to_ref")
        val Nodes_Refs_to_Chrom_ID = Nodes_chroms.join(Edges_chromToRef, Nodes_chroms("id") === Edges_chromToRef("src")).select("dst")

        val Nodes_Refs_in_Range = g.vertices.filter($"nodetype" === "Ref_allele" && $"start" > "1" && $"start" < 50000000)
        val Nodes_Refs_to_Chrom_in_Range = Nodes_Refs_in_Range.join(Nodes_Refs_in_Range, Nodes_Refs_in_Range("id") === Nodes_Refs_in_Range("id"))

        val paths = g.find("(a)-[e]->(b)").filter($"a.nodetype" === "Chrom").filter("e.relationship = 'Chrom_to_ref'").filter($"b.nodetype" === "Ref_allele").count
    }
}