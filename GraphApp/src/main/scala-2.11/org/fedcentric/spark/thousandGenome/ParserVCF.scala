/**
  * Created by Tianwen on 7/29/2016.
  */
package org.fedcentric.spark.thousandGenome

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.fedcentric.spark.annotation.{Input, OutputFun}
import org.fedcentric.spark.graph.Parser

import scala.collection.Map
import scala.collection.mutable.ArrayBuffer

class ParserVCF(@Input(value = "LoadedData", valueType = "self-defined VCF")
                val input: String,
                val sparksess: SparkSession
               )extends Parser{

    /**
     * Output function which will be called in main() in the app, taking input
     * as loaded data, then return a parsed structure of data.
     * @return The parsed result
     */
    @OutputFun(value = "ParsedData", valueType = "RDD[ParsedLine]")
    def Parse(): RDD[ParsedLine] = {
        val RDD_RawData = sparksess.sparkContext.textFile(input)
        val VCF_Data = ReadHeader(RDD_RawData)
        //val VCFBody_Parsed_RDD = VCF_Data.VCFBody.map(line => ParseLine(line, VCF_Data.InfoKeys, VCF_Data.SampleIds)).cache()

        implicit val InfoKeys = VCF_Data.InfoKeys
        implicit val SampleIds = VCF_Data.SampleIds
        val VCFBody_Parsed_RDD = VCF_Data.VCFBody.mapPartitions(helperParseLine).cache
        println("-------------------Done Parsing-------------------")
        VCFBody_Parsed_RDD
    }

    /**
     * Parse the info columns in each line, most of them appear as key=value which
     * is separated by ';', some of keys have multiple values(equal or smaller than
     * the number of alt alleles), some of keys have single value or no value.
     * @param Info_column element in each line, which contains a lot of key=value
     * @return All the Key->value pairs.
     */
    @inline
    private def ParseInfo(Info_column: String, Info_Ref:Set[String]): InfoColumnDict = {
        var info_dict:InfoColumnDict = Map[String, Array[String]]()
        val Info_items = Info_column.split(";")
        // iterating all the info keys
        for (info_item <- Info_items) {
            val key_values = info_item.split("=")
            // checking if this current info key is contained in the meta data(header in VCF)
            if (Info_Ref.contains(key_values(0))) {
                if (key_values.length == 1) {
                    // "1" means true, "0" means false
                    info_dict += (key_values(0) -> Array("1"))
                }else if(key_values.length != 0){
                    val values = key_values(1).split(",")
                    info_dict += (key_values(0) -> values)
                }
            }
        }
        return info_dict
    }

    /**
     * Parse the individual genotype columns. Only extracting individual who has
     * alt allele in his/her genotypes. Ordering
     * @param sample_data Array of genotypes for all samples
     * @param sample_ids Array of sample ID, eg, HG00096
     * @param Num_alt_alleles Number of alt allele in this line
     * @return
     */
    @inline
    private def ParseSampleData(sample_data: Array[String],
                                sample_ids: Array[String],
                                Num_alt_alleles: Int) :
    (Allele_Ind_homo, Allele_Ind_hete)= {
        
        val Ind_variants_homo:Allele_Ind_homo = Array.fill(Num_alt_alleles) {
            new ArrayBuffer[String]()
        }
        val Ind_variants_hete:Allele_Ind_hete = Array.fill(Num_alt_alleles) {
            new ArrayBuffer[String]()
        }
        
        val homo_ref = "0|0"
        val chromY_ref = "0"
        
        var id_var_0 = 0
        var id_var_1 = 0
        
        for (id <- 0 until sample_data.length){
            val sample = sample_data(id).replaceAll("\n","")
            // May be "0", "0|1", "1|0", "1|1", "1|0|0"
            if(sample != homo_ref){
                val genotypes = sample.split('|')
                if(genotypes.length == 2){
                    // Hetero
                    if(genotypes(0) != genotypes(1)){
                        id_var_0 = genotypes(0).toInt - 1
                        id_var_1 = genotypes(1).toInt - 1
                        if(id_var_0 >= 0){
                            Ind_variants_hete(id_var_0).append(sample_ids(id))
                        }
                        if(id_var_1 >= 0){
                            Ind_variants_hete(id_var_1).append(sample_ids(id))
                        }
                    }else{
                        id_var_0 = genotypes(0).toInt - 1
                        Ind_variants_homo(id_var_0).append(sample_ids(id))
                    }
                }
                // if not equals to "0" for chrom Y, then count them
                // into hetero_var_dict
                // sometimes, there will be "." in chrom Y data
                else if(genotypes.length == 1 &&
                    sample != chromY_ref &&
                    sample != "." &&
                    genotypes(0).matches("[0-9]")) {
                    id_var_0 = genotypes(0).toInt - 1
                    Ind_variants_hete(id_var_0).append(sample_ids(id))
                }
            }
        }
        (Ind_variants_homo, Ind_variants_hete)
    }

    /**
     * The method which will be used in spark map method. Parse each column in a row
     * In a row, 0: Chromosome
     *           1: Starting location of Ref allele
     *           3: Reference allele column
     *           4: Alt allele column
     *           7: Information column
     *           9 to the end: individual genotype columns
     * @param row
     * @return
     */
    private def ParseLine(row: String,
                          Info_column_broadcast:org.apache.spark.broadcast.Broadcast[Set[String]],
                          SampleIds_broadcast:org.apache.spark.broadcast.Broadcast[Array[String]]
                         ): ParsedLine = {
        
        val columns = row.split("\t")
        val Chrom = columns(0)
        val Ref_Start_pos = columns(1)
        val Ref_allele = columns(3)
        val Ref_Stop_pos = Ref_Start_pos.toInt + Ref_allele.length - 1
        val Ref_info = Map[String, String](
            "Chrom" -> Chrom,
            "Ref_Start_pos" -> Ref_Start_pos,
            "Ref_allele" -> Ref_allele,
            "Ref_Stop_pos" -> Ref_Stop_pos.toString)
        
        val record_list = new ArrayBuffer[(Map[String, String], Array[String], Array[String])]()
        val Alt_Alleles = columns(4)
        if ( Alt_Alleles == ".") {
            //println("There is missing alt allele in current line")
        } else{
            // cases that Alt_allele_Column having '<' or '>'
            val Alt_allele_Column = columns(4).replaceAll("<", "[").replaceAll(">", "]")
            val Alt_alleles = Alt_allele_Column.split(",")
            val Num_alt_alleles = Alt_alleles.length
            val Info_column = columns(7)
            val Info_dict = ParseInfo(Info_column, Info_column_broadcast.value)
            val results = ParseSampleData(columns.slice(9, columns.length), SampleIds_broadcast.value, Num_alt_alleles)
            val homo_list = results._1
            val hetero_list = results._2
            for (variant_index <- 0 until Alt_alleles.length) {
                var variant_info = Map[String, String]()
                variant_info += ("Alt_alleles" -> Alt_alleles(variant_index))
                
                for (infoPair <- Info_dict) {
                    val values = infoPair._2
                    if (variant_index < values.length) {
                        variant_info += (infoPair._1 -> values(variant_index))
                    }
                }
                if (Alt_alleles(variant_index).length > 1) {
                    variant_info += ("VT" -> "INDEL")
                } else {
                    variant_info += ("VT" -> "SNP")
                }
                record_list.append((variant_info, hetero_list(variant_index).toArray, homo_list(variant_index).toArray))
            }
        }
        return (Ref_info, record_list.toArray)
    }

    def helperParseLine(iterator:Iterator[String])(implicit Info_column_broadcast:org.apache.spark.broadcast.Broadcast[Set[String]],
                                                   SampleIds_broadcast:org.apache.spark.broadcast.Broadcast[Array[String]]):Iterator[ParsedLine] = {
        val res = new ArrayBuffer[ParsedLine]()
        res.sizeHint(4000)

        var duplicate_check:String = ""
        var cur_signature = ""
        while(iterator.hasNext){
            val row = iterator.next()
            // skip the header
            if(!row.startsWith("#")){
                val columns = row.split("\t")
                val Chrom = columns(0)
                val Ref_Start_pos = columns(1)
                val Ref_allele = columns(3)
                val Ref_Stop_pos = Ref_Start_pos.toInt + Ref_allele.length - 1
                cur_signature = "%s:%s:%s".format(Chrom, Ref_Start_pos, Ref_allele)
                val Ref_info = if(cur_signature != duplicate_check){
                    duplicate_check = cur_signature
                    Map[String, String]("Chrom" -> Chrom, "Ref_Start_pos" -> Ref_Start_pos, "Ref_allele" -> Ref_allele, "Ref_Stop_pos" -> Ref_Stop_pos.toString)
                } else {
//                    Map[String, String]()
                    Map[String, String]("Chrom" -> Chrom, "Ref_Start_pos" -> Ref_Start_pos, "Ref_allele" -> Ref_allele, "Ref_Stop_pos" -> Ref_Stop_pos.toString)

                }

                val record_list = new ArrayBuffer[(Map[String, String], Array[String], Array[String])]()
                record_list.sizeHint(4)

                val Alt_Alleles = columns(4)
                if (Alt_Alleles == ".") {
                    //println("There is missing alt allele in current line")
                } else {
                    // cases that Alt_allele_Column having '<' or '>'
                    val Alt_allele_Column = columns(4).replaceAll("<", "[").replaceAll(">", "]")
                    val Alt_alleles = Alt_allele_Column.split(",")
                    val Num_alt_alleles = Alt_alleles.length
                    val Info_column = columns(7)
                    val Info_dict = ParseInfo(Info_column, Info_column_broadcast.value)
                    val results = ParseSampleData(columns.slice(9, columns.length), SampleIds_broadcast.value, Num_alt_alleles)
                    val homo_list = results._1
                    val hetero_list = results._2
                    for (variant_index <- 0 until Alt_alleles.length) {
                        var variant_info = Map[String, String]()
                        variant_info += ("Alt_alleles" -> Alt_alleles(variant_index))

                        for (infoPair <- Info_dict) {
                            val values = infoPair._2
                            if (variant_index < values.length) {
                                variant_info += (infoPair._1 -> values(variant_index))
                            }
                        }
                        if (Alt_alleles(variant_index).length > 1) {
                            variant_info += ("VT" -> "INDEL")
                        } else {
                            variant_info += ("VT" -> "SNP")
                        }
                        record_list.append((variant_info, hetero_list(variant_index).toArray, homo_list(variant_index).toArray))
                    }
                }
                res.append((Ref_info, record_list.toArray))
            }
        }
        res.toArray.iterator
    }

    @inline
    @transient
    private def ReadHeader(SourceRDD : RDD[String]): VCF = {
        /*
        // old version of filtering header
        val DataInfo = SourceRDD.filter(line => line.startsWith("##"))
        val DataInfoKeys = DataInfo.filter(line => line.startsWith("##INFO")).map(_.split("=")(2)).map(_.split(",")(0)).collect().toSet
        */
        val DataInfoKeys = SourceRDD.mapPartitionsWithIndex(helpParseHeader).collect().toSet
        val DataInfoKeys_BroadCast = sparksess.sparkContext.broadcast(DataInfoKeys)
        // Extract body part including the row of column names
        val DataBodyWithHeader = SourceRDD.filter(line => !line.startsWith("##"))
        val HeaderLine = DataBodyWithHeader.first()
        val column_names = HeaderLine.split("\t")
        val Sample_ids = column_names.slice(9, column_names.length)
        val SampleIds_BroadCast = sparksess.sparkContext.broadcast(Sample_ids)

        // Extract  body
        val DataBody = DataBodyWithHeader.filter(line => !line.startsWith("#"))
        VCF(DataInfoKeys_BroadCast, SampleIds_BroadCast, HeaderLine, DataBody)
    }

    /**
     * All the header information is stored in the first partition, so just doing filter on the first partition will greatly
     * improve the speed
     * @param indexPartition index of rdd partition
     * @param iterator
     * @return
     */
    def helpParseHeader(indexPartition:Int, iterator:Iterator[String]):Iterator[String] = {
        if(indexPartition == 0){
            val tmp = iterator.toArray
            tmp.filter(line => line.startsWith("##INFO")).map(_.split("=")(2)).map(_.split(",")(0)).iterator
        }else{
            Iterator()
        }
    }

}