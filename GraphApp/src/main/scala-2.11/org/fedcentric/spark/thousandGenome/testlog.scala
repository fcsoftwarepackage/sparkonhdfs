import org.apache.log4j.{Level, LogManager}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

trait Logger extends Serializable{
    @transient lazy val log = LogManager.getLogger("myLogger")

    def doSomeMappingOnDataSetAndLogIt(rdd: RDD[Int]): RDD[String] =
        rdd.map{ i =>
            log.warn("mapping: " + i)
            (i).toString
        }
}

object Logger {
    def apply(n: Int) = println("Hi")
}

object Test extends Logger{
    def main(args: Array[String]) {

        val spark = SparkSession.builder().appName("demo-log-app").getOrCreate()
        val sc = spark.sparkContext

        log.setLevel(Level.DEBUG)

        val data = sc.parallelize(1 to 100000)

        log.warn("Hello demo")
        log.warn("I am super man")

        val other = doSomeMappingOnDataSetAndLogIt(data)
        other.collect()

        log.warn("I am done")
        spark.stop()
    }
}
