Two arguments should add to the sparksubmit

1). Path(Either HDFS or Local Disk) of Source Data
2). Path(Either HDFS or Local Disk) to store graph table

Example:

/opt/spark-2.0/bin/spark-submit \
            --name "Cluster Test of Graph" \
            --class "org.fedcentric.spark.thousandGenome.RunGraph" \
            --master spark://10.1.10.40:7077 \
            --driver-memory 25G \
            --driver-cores 4 \
            --total-executor-cores $(($idx*12)) \
            --executor-cores 1 \
            --executor-memory 50G \
            --conf spark.driver.maxResultSize=20G \
            --conf spark.driver.cores=1 \
            --conf spark.driver.memory=5g \
            --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
            --conf spark.kryoserializer.buffer.max=256m \
            --packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 \
	    target/scala-2.11/graphapp_2.11-1.0.jar \
            hdfs://10.1.10.40/VCFs hdfs://10.1.10.40/GraphStorage